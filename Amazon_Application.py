__author__ = 'Asia'
"""
The following script uses Selenium, Beautiful Soup and Xvfb.

TO DO:
1. Add £ to the price displayed in email
2. Improve def send_email() so that both emails don't appear in duplicate
3. Make Cron notify users about encountered errors
4. Remove email addresses to improve security
"""
from selenium import webdriver
from bs4 import BeautifulSoup
from pyvirtualdisplay import Display
import smtplib

def download_webpage(required_page):
    #use Xvfb to hide browser's window from showing
    display = Display(visible = 0, size = (800,600))
    display.start()

    #use Selenium to open the page and extract source code
    open_Firefox = webdriver.Firefox()
    open_Firefox.get(required_page)

    #extract new prices (dp-new-col) and used prices (dp-used-col) and combine together in both_elements
    new_elements = open_Firefox.find_elements_by_class_name("dp-new-col")
    used_elements = open_Firefox.find_elements_by_class_name("dp-used-col")
    both_elements = new_elements + used_elements

    inner_list = []
    #note that both_elements contains html so the code below extracts text from both_elements;
    #however extracted text still contains <span></span>
    for word in both_elements:
        inner = word.get_attribute("innerHTML")
        inner_list.append(inner)

    print ('Firefox is about to quit')
    open_Firefox.quit()
    display.stop()
    print ('Firefox stopped, returning the downloaded webpage')
    return inner_list

def send_email(subject, text):

    From = "misjoginotifier@gmail.com"
    To = ["joczerw@gmail.com", "bieniekmat@gmail.com"]
    Pwn = "#Piknik#"

    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    server.starttls()
    server.login(From,Pwn)

    msg = "\r\n".join([
    "From: " + From,
    "To: joczerw@gmail.com, bieniekmat@gmail.com",
    "Subject: " + subject,
    "",
    text
    ])

    server.sendmail(From,To,msg)
    server.quit()

def get_lowest_price(html_text):
    #convert html_text (type: list) into str, parse using Beautiful Soup and return a look_for_prices (type: list)
    str_html = " ".join(html_text)
    inner_text_bs = BeautifulSoup(str_html)
    look_for_price = inner_text_bs.find_all("span")

    prices = []
    #modify each element in look_for_price to remove £ and save each price as a float in prices
    for element in look_for_price:
        modified_element = element.text
        no_pound = modified_element.replace("£", "")
        float_no_pound = float(no_pound)
        prices.append(float_no_pound)

    lowest_price = min(prices)
    return lowest_price

def load_books_and_prices():
    #list of books and preferred prices should be saved in "My_books.txt"
    #full name of the directory needs to be given to run the script with Cron
    f = open("/home/dresio/software/amazon-application/My_books.txt")
    content = f.read()
    f.close()

    split_file = content.split()
    #programme always expects to have an even number of elements in "My_books.txt", i.e. a webpage and a price
    assert (len(split_file) % 2 == 0), "You're missing the information in the file"
    return split_file

if __name__ == "__main__":
    print ("Aplikacja Amazon zaczyna skanowac amazon.co.uk w poszukiwaniu niskich cen")
    webpage_and_price = load_books_and_prices()

    i = 0
    #extract information from "My_books.txt" (line by line) and run the code to see if price has gone down
    while i < len(webpage_and_price):
        webpage = webpage_and_price[i]
        print ('Nastepna ksiazka do sprawdzenia to: ', webpage)
        preferred_price = float(webpage_and_price[i+1])

        html_list = download_webpage(webpage)
        lowest_price = get_lowest_price(html_list)

        if lowest_price < preferred_price:
            send_email("The price of the book has dropped!", "The book from: " + webpage + " costs now " + str(lowest_price) + ".")

        i += 2
